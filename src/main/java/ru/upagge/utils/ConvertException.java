package ru.upagge.utils;

/**
 * @author upagge 02.04.2021
 */
public class ConvertException extends RuntimeException {

    public ConvertException(String message) {
        super(message);
    }

}

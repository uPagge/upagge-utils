package ru.upagge.utils;

import lombok.NonNull;
import lombok.experimental.UtilityClass;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Утилитарный класс для работы с объектами.
 *
 * @author upagge 07.03.2021
 */
@UtilityClass
public class ObjectUtils {

    /**
     * <p>Позволяет получить значение вложенного в объект поля по названию поля.</p>
     * <p></p>
     * <p><b>При этом у поля должен быть доступен get-метод</b></p>
     * <p></p>
     * <p>Поддерживаются воженные объекты, чтобы задать такой объект, необходимо прописать путь до него от переданного объекта через '.'. У всех объектов на пути должны быть get-методы.</p>
     *
     * @param fieldName Имя поля в объекте или цепь имен
     * @param object    Объект
     * @return Значение поля из объекта
     */
    public static Object getFieldValue(@NonNull Object object, @NonNull String fieldName) {
        int firstNameIndex = fieldName.indexOf(".");
        String firstName;
        if (firstNameIndex != -1) {
            firstName = fieldName.substring(0, firstNameIndex);
        } else {
            firstName = fieldName;
        }
        for (Method method : object.getClass().getMethods()) {
            if (isGetMethod(firstName, method)) {
                try {
                    final Object invoke = method.invoke(object);
                    if (firstNameIndex == -1) {
                        return invoke;
                    } else {
                        return getFieldValue(invoke, fieldName.substring(firstNameIndex + 1));
                    }
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new ConvertException("Не удалось определить метод " + method.getName());
                }
            }
        }
        throw new ConvertException("Метод у объекта не найден");
    }

    /**
     * Определяет что метод является геттером поля.
     *
     * @param fieldName Поле
     * @param method    Метод
     * @return true если метод является геттером для fieldName, false в противном случае
     */
    public static boolean isGetMethod(@NonNull String fieldName, Method method) {
        return (method.getName().startsWith("get"))
                && (method.getName().length() == (fieldName.length() + 3))
                && method.getName().toLowerCase().endsWith(fieldName.toLowerCase());
    }

    public static <T> Collection<T> toCollect(T... t) {
        return Arrays.stream(t).collect(Collectors.toList());
    }

}
